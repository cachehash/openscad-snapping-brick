brick(4,2);
res=250;
bitw=8;
bith=9.6;
wall=1.6;
bracewall=wall;
knobh=wall;
knobr=bitw/2-wall;
module ccube(x,y,z) {
	translate([-x/2,-y/2,0]) {
		cube([x,y,z]);
	}
}
module cutcube(w,l) {
	x=bitw*w;
	y=bitw*l;
	translate([x/2,y/2,0]) {
		difference() {
			ccube(x,y,bith);
			translate([0,0,-wall]) {
				ccube(x-2*wall, y-2*wall, bith);
			}
		}
	}
}
module knob(x,y,r=knobr,h=knobh) {
	translate([x*bitw - bitw/2,y*bitw - bitw/2,bith]) {
		cylinder(h,r,r, $fn = res);
	}
}
module knobs(w,l,r=knobr,h=knobh) {
	for (x = [1:w]) {
		for (y = [1:l]) {
			knob(x,y,r,h);
		}
	}
}
module brace(x,y) {
	r=sqrt(2)*bitw/2-knobr;
	translate([x*bitw, y*bitw, 0]) {
		difference() {
			cylinder(bith,r,r, $fn=res);
			translate([0,0,-bith/2]) {
				cylinder(2*bith,r-bracewall/2,r-bracewall/2, $fn=res);
			}
		}
	}
}
module braces(w,l) {
	if (w > 1 && l > 1) {
		for (x = [1:w-1]) {
			for (y = [1:l-1]) {
				brace(x,y);
			}
		}
	}
}
module brick(w, l) {
	difference() {
		cutcube(w,l);
		translate([0,0,-2*knobh]) {
			knobs(w,l,knobr-wall,2*knobh);
		}
	}
	knobs(w,l);
	braces(w,l);
}
